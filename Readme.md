# Replication Package for the Paper on
## [An Empirical Study of the Effect of File Editing Patterns on Software Quality?](http://feng-zhang.com/?p=145)

### Anything unclear, please don't hestitate to contact any of the authors.
--------------------------------------------------------------------------------

## 1. Authors

* [Feng Zhang](http://www.feng-zhang.com) (first name <at> cs.queensu.ca)
* [Foutse Khomh](http://www.khomh.net) (first name <dot> last name <at> polymtl.ca)
* [Ying Zou](http://post.queensu.ca/~zouy) (first name <dot> last name <at> queensu.ca)
* [Ahmed E. Hassan](http://www.cs.queensu.ca/~ahmed/home) (first name <at> cs.queensu.ca)

## 2. Data Sources

Our empirical study uses Eclipse projects as our subject projects.
We have two types of data sources: bug report and cvs repositories. Both are publically avaiable:

### 2.1 Bug reports are collected on [Eclipse Bugzilla](https://bugs.eclipse.org/bugs).

### 2.2 CVS repositories are downloaded from [Eclipse Archives](http://archive.eclipse.org/arch).

## 3. Dataset and R Script

The original dataset of this work can be download from [here](https://bitbucket.org/serap/fileeditingpatternstudy/src/3b298535348e80a27fac83462ba02db3a8eaa901/fileeditingpatternstudy.zip?at=master).
However, it contains many useless information because this is my first project.
While doing this project, I started to learn R programming, data processing, metric computing, and etc.
As a result, there are some useless 'history' in my dataset, e.g., source code metrics.
And my old R script is too difficult to understand.
Any way the revised dataset can be found [here](https://bitbucket.org/serap/fileeditingpatternstudy/src/4625469910f816ebe6e5cd6745202971981bafb3/fileEditingPatternDataset.csv?at=master).
The revised R script can be found [here](https://bitbucket.org/serap/fileeditingpatternstudy/src/4625469910f816ebe6e5cd6745202971981bafb3/wcre2012_patterns.R?at=master).

## 4. Reference

If you think the dataset is not enough and you want the original bug reports that contain Mylyn logs, I would also like to share with you.
Anyone is welcome to use this dataset.
If you use our data in your work, please consider to cite our paper:

### 4.1 Plain Text
> Feng Zhang, Foutse Khomh, Ying Zou, and Ahmed E. Hassan, **An Empirical Study of the Effect of File Editing Patterns on Software Quality**, Proceedings of the 19th Working Conference on Reverse Engineering (WCRE'12), October 15-18, 2012, Kingston, Ontario, Canada. IEEE Computer Society Press.

### 4.2 BibTex Entry
    @inproceedings{zkzh-wcre-2012,
    author = {Zhang, Feng and Khomh, Foutse and Zou, Ying and Hassan, Ahmed E.},
    title = {An Empirical Study of the Effect of File Editing Patterns on Software Quality},
    booktitle = {Proceedings of the 19th Working Conference on Reverse Engineering},
    series = {WCRE '12},
    year = {2012},
    numpages = {10},
    keywords = {file editing pattern, change request, bug, software quality, empirical software engineering, mylyn},
    }