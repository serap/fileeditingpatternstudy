####################################################################
# Copyright (c) 2011-2013 by the original author and all contributors
# of this file. All rights reserved.
#
# Author: Feng Zhang (feng-zhang.com)
# Date  : 2013-09-30
# Note  : This work is a part of my research during my Ph.D study at
#         Software Reenginnering Lab of Queen's University at
#         Kingston, Ontario, Canada.
# 
# History:
#    (Format: Version, Date, Developer, Comments)
#    1.0, 2013-09-30, Feng Zhang (feng-zhang.com), Add this header.
####################################################################

library("gplots")
library("e1071")   # for skewness

####################################################################
## Read in data file
####################################################################
# header:
# NTotalEdits,NConcurrentEdits,NParallelEdits,EditTime,IdleTime,codeChurn,LOC,postBugFixes6Months
fileEditingPatternData = read.csv("fileEditingPatternDataset.csv")

####################################################################
## Read the metrics
####################################################################
# module information
module = fileEditingPatternData$module

# Concurrent Pattern Metrics
NTotalEdits = fileEditingPatternData$NTotalEdits
NConcurrentEdits = fileEditingPatternData$NConcurrentEdits
NConcurrentEdits = NConcurrentEdits/NTotalEdits

# Parallel Pattern Metrics
NParallelEdits = fileEditingPatternData$NParallelEdits

# Extended Pattern Metrics
EditTime = fileEditingPatternData$EditTime

# Interrupted Pattern Metrics
IdleTime = fileEditingPatternData$IdleTime

# code churn
codeChurn =  fileEditingPatternData$codeChurn

# lines of code
LOC = fileEditingPatternData$LOC

# density of post bugs in the six months
post = fileEditingPatternData$postBugFixes6Months
post = post/LOC

####################################################################
## Identify files with patterns
####################################################################
# 1. Compute thresholds to identify each pattern
thresholdCE = 1
thresholdPE = 1
thredholdEE = quantile(EditTime)[4]
thredholdIE = quantile(IdleTime)[4]

# 2. Get indices of files with each pattern
indices = c(1:length(NConcurrentEdits))
indices_CE = indices[NConcurrentEdits>thresholdCE]
indices_PE = indices[NParallelEdits>thresholdPE]
indices_EE = indices[EditTime>=thredholdEE]
indices_IE = indices[IdleTime>=thredholdIE]

####################################################################
## RQ1. Are there different file editing patterns?
####################################################################
print("RQ1. Are there different file editing patterns?")
# 1. no pattern
indices_NoP = indices[-c(indices_CE, indices_PE, indices_EE, indices_IE)]
# 2. two patterns
indices_CE_PE = intersect(indices_CE, indices_PE)
indices_CE_EE = intersect(indices_CE, indices_EE)
indices_CE_IE = intersect(indices_CE, indices_IE)
indices_PE_EE = intersect(indices_PE, indices_EE)
indices_PE_IE = intersect(indices_PE, indices_IE)
indices_EE_IE = intersect(indices_EE, indices_IE)
# 3. three patterns
indices_CE_PE_EE = intersect(indices_CE_PE, indices_EE)
indices_CE_PE_IE = intersect(indices_CE_PE, indices_IE)
indices_CE_EE_IE = intersect(indices_CE_EE, indices_IE)
indices_PE_EE_IE = intersect(indices_PE_EE, indices_IE)
# 4. four patterns
indices_CE_PE_EE_IE = intersect(indices_CE_PE_EE, indices_IE)
print(paste(length(indices_NoP), length(indices_CE), length(indices_PE), length(indices_EE), length(indices_IE), length(indices_CE_PE), length(indices_CE_EE), length(indices_CE_IE), length(indices_PE_EE), length(indices_PE_IE), length(indices_EE_IE), length(indices_CE_PE_EE), length(indices_CE_PE_IE), length(indices_CE_EE_IE), length(indices_PE_EE_IE), length(indices_CE_PE_EE_IE)))

####################################################################
## RQ2. Do file editing patterns lead to more bugs?
####################################################################
print("RQ2. Do file editing patterns lead to more bugs?")

# define a function for comparing occurrences
compareOccurrenceByIndices <- function(bug, titleA, indicesA, titleB, indicesB)
{
	bug_A = bug[indicesA]
	bug_B = bug[indicesB]
	#     formulate the matrix to test
	matrix_A_B = c(length(bug_A[bug_A>0]), length(bug_A[bug_A==0]), length(bug_B[bug_B>0]), length(bug_B[bug_B==0])) # noPattern_with_bugs, noPattern_without_bugs, pattern_with_bugs, pattern_without_bugs
	dim(matrix_A_B) = c(2, 2)
	#     fisher exact test
	fisher_p = fisher.test(matrix_A_B)$p.value
	#     compute odds ratio
	oddsRatio = (matrix_A_B[1, 2]/matrix_A_B[2, 2])/(matrix_A_B[1, 1]/matrix_A_B[2, 1])
	meanIncreasedTimes = mean(bug_B)/mean(bug_A)
	print(paste("Comparing ", titleA, " v.s ", titleB, ":", sep=""))
	print(paste("    odds ratio=", sprintf("%.3f", oddsRatio), " (p-value=",  sprintf("%.2e", fisher_p), ", mean increased by ", sprintf("%.3f", meanIncreasedTimes), ")", sep=""))
}

#-------------------------------------------------------------------
# 1. Compare the occurrence of files with v.s without pattern
print("RQ2.1 Compare the occurrence of files with v.s without pattern.")
#-----------------------
# 1.1 Concurrent Editing
indices_NoCE = indices[-indices_CE]
compareOccurrenceByIndices(post, "NonCE", indices_NoCE, "CE", indices_CE)
#-----------------------
# 1.2 Parallel Editing
indices_NoPE = indices[-indices_PE]
compareOccurrenceByIndices(post, "NonPE", indices_NoPE, "PE", indices_PE)
#-----------------------
# 1.3 Extended Editing
indices_NoEE = indices[-indices_EE]
compareOccurrenceByIndices(post, "NonEE", indices_NoEE, "EE", indices_EE)
#-----------------------
# 1.4 Interrupted Editing
indices_NoIE = indices[-indices_IE]
compareOccurrenceByIndices(post, "NonIE", indices_NoIE, "IE", indices_IE)
#-------------------------------------------------------------------
# 2. Compare the occurrence of files with different levels of pattern v.s without pattern
print("RQ2.2 Compare the occurrence of files with different levels of pattern v.s without pattern.")
#-----------------------
# 2.1 Concurrent Editing
NConcurrentEdits_CE = NConcurrentEdits[indices_CE]
indices_CE_low = indices_CE[NConcurrentEdits_CE<quantile(NConcurrentEdits_CE)[4]]
indices_CE_high = indices_CE[NConcurrentEdits_CE>=quantile(NConcurrentEdits_CE)[4]]
compareOccurrenceByIndices(post, "NonCE", indices_NoCE, "low CE", indices_CE_low)
compareOccurrenceByIndices(post, "NonCE", indices_NoCE, "hight CE", indices_CE_high)
# 2.2 Parallel Editing
NParallelEdits_PE = NParallelEdits[indices_PE]
indices_PE_low = indices_PE[NParallelEdits_PE<quantile(NParallelEdits_PE)[4]]
indices_PE_high = indices_PE[NParallelEdits_PE>=quantile(NParallelEdits_PE)[4]]
compareOccurrenceByIndices(post, "NonPE", indices_NoPE, "low PE", indices_PE_low)
compareOccurrenceByIndices(post, "NonPE", indices_NoPE, "hight PE", indices_PE_high)
# 2.3 Extended Editing
indices_EE_non = indices[EditTime<quantile(EditTime)[3]] # less than median
indices_EE_low = indices[EditTime>=quantile(EditTime)[3] & EditTime<quantile(EditTime)[4]] # median to 75%
indices_EE_high = indices[EditTime>=quantile(EditTime)[4]] # greater than 75%
compareOccurrenceByIndices(post, "NonEE", indices_EE_non, "low EE", indices_EE_low)
compareOccurrenceByIndices(post, "NonEE", indices_EE_non, "hight EE", indices_EE_high)
# 2.4 Interrupted Editing
indices_IE_non = indices[IdleTime<quantile(IdleTime)[3]] # less than median
indices_IE_low = indices[IdleTime>=quantile(IdleTime)[3] & IdleTime<quantile(IdleTime)[4]] # median to 75%
indices_IE_high = indices[IdleTime>=quantile(IdleTime)[4]] # greater than 75%
compareOccurrenceByIndices(post, "NonIE", indices_IE_non, "low EE", indices_IE_low)
compareOccurrenceByIndices(post, "NonIE", indices_IE_non, "hight EE", indices_IE_high)

####################################################################
## RQ3. Do interactions among file editing patterns lead to more bugs?
####################################################################
print("RQ3. Do interactions among file editing patterns lead to more bugs?")
#  single pattern v.s no pattern
compareOccurrenceByIndices(post, "None Pattern", indices_NoP, "CE", indices_CE)
compareOccurrenceByIndices(post, "None Pattern", indices_NoP, "PE", indices_PE)
compareOccurrenceByIndices(post, "None Pattern", indices_NoP, "EE", indices_EE)
compareOccurrenceByIndices(post, "None Pattern", indices_NoP, "IE", indices_IE)
#  two pattern v.s no pattern
compareOccurrenceByIndices(post, "None Pattern", indices_NoP, "CE_PE", indices_CE_PE)
compareOccurrenceByIndices(post, "None Pattern", indices_NoP, "CE_EE", indices_CE_EE)
compareOccurrenceByIndices(post, "None Pattern", indices_NoP, "CE_IE", indices_CE_IE)
compareOccurrenceByIndices(post, "None Pattern", indices_NoP, "PE_EE", indices_PE_EE)
compareOccurrenceByIndices(post, "None Pattern", indices_NoP, "PE_IE", indices_PE_IE)
compareOccurrenceByIndices(post, "None Pattern", indices_NoP, "EE_IE", indices_EE_IE)
#  three pattern v.s no pattern
compareOccurrenceByIndices(post, "None Pattern", indices_NoP, "CE_PE_EE", indices_CE_PE_EE)
compareOccurrenceByIndices(post, "None Pattern", indices_NoP, "CE_PE_IE", indices_CE_PE_IE)
compareOccurrenceByIndices(post, "None Pattern", indices_NoP, "CE_EE_IE", indices_CE_EE_IE)
compareOccurrenceByIndices(post, "None Pattern", indices_NoP, "PE_EE_IE", indices_PE_EE_IE)
#  four pattern v.s no pattern
compareOccurrenceByIndices(post, "None Pattern", indices_NoP, "CE_PE_EE_IE", indices_CE_PE_EE_IE)

####################################################################
## Show the dataset boxplot
####################################################################
pdf("dataset_boxplot.pdf", width=5, height=4)
boxplot(post[module=="Mylyn"], post[module=="Platform"], post[module=="PDE"], post, names=c("Mylyn", "Platform", "PDE", "All"), ylab="Future Bug Density")#, cex.lab=1.4, cex.main=2,cex.axis=2, mgp=c(-10, -10, -10))
dev.off()

####################################################################
## Draw histogram of four patterns
####################################################################
scale = 2
myMGP = c(2.6,0.8,0)
pdf("histCE.pdf")
hist(log(NConcurrentEdits), xlab="log(NConcurrentEdits)",main="", ylab="# of Files",cex.lab=scale,cex.main=scale,cex.axis=scale, xlim=c(0,max(log(NConcurrentEdits))), mgp=myMGP)
box()
dev.off()
pdf("histPE.pdf")
hist(log(NParallelEdits), xlab="log(NParallelEdits)",main="", ylab="# of Files",cex.lab=scale,cex.main=scale,cex.axis=scale, xlim=c(0,max(log(NParallelEdits))), mgp=myMGP)
box()
dev.off()
pdf("histEE.pdf")
hist(log(EditTime+1), xlab="log(EditTime+1)",main="", ylab="# of Files",cex.lab=scale,cex.main=scale,cex.axis=scale, xlim=c(0,max(log(EditTime+1))), mgp=myMGP)
box()
dev.off()
pdf("histIE.pdf")
hist(log(IdleTime+1), xlab="log(IdleTime+1)",main="", ylab="# of Files",cex.lab=scale,cex.main=scale,cex.axis=scale, xlim=c(0,max(log(IdleTime+1))), mgp=myMGP)
box()
dev.off()

####################################################################
## Show thresholds of our metrics
####################################################################
print("Quantile of EditTime")
print(quantile(EditTime))
print("Quantile of IdleTime")
print(quantile(IdleTime))



